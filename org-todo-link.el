;;; org-todo-link.el --- Show status of any linked todo item  -*- lexical-binding: t -*-

;; Copyright (C) 2014-2023 Free Software Foundation, Inc.

;; Author: Amy Grinn <grinn.amy@gmail.com>
;; Package-Requires: ((emacs "28.1"))
;; Version: 1.0.0
;; URL: https://gitlab.com/grinn.amy/org-todo-link

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Activate the global mode `org-todo-link-mode' in order to display
;; the todo keyword before any `file' link where the target is a todo
;; item.

;; With point inside such a link, use C-c C-t (`org-todo-link-todo')
;; to set the todo keyword on the target heading, and C-c C-c
;; (`org-todo-link-recalculate') to recalculate the todo keyword.

;; The keyword is displayed as a clickable button, which will allow
;; you to set the todo keyword on the target heading via the mouse.

;;; Code:

;;;; Requirements:

(require 'ol)
(require 'org-refile)
(require 'org-element)

;;;; Options

(defgroup org-todo-link nil
  "Customization options for todo-style links."
  :group 'applications)

(defcustom org-todo-link-always-display nil
  "Display button for broken links and target headings without a todo keyword."
  :group 'org-todo-link
  :type 'boolean)

(defcustom org-todo-link-completion-targets nil
  "Targets for finding a TODO item.

See `org-refile-targets' for specification.

If nil, uses `org-refile-targets'."
  :group 'org-todo-link
  :type '(repeat
          (cons
           (choice :value org-agenda-files
                   (const :tag "All agenda files" org-agenda-files)
                   (const :tag "Current buffer" nil)
                   (function) (variable) (file))
           (choice :tag "Identify target headline by"
                   (cons :tag "Specific tag" (const :value :tag) (string))
                   (cons :tag "TODO keyword" (const :value :todo) (string))
                   (cons :tag "Regular expression" (const :value :regexp) (regexp))
                   (cons :tag "Level number" (const :value :level) (integer))
                   (cons :tag "Max Level number" (const :value :maxlevel) (integer))))))

(defcustom org-todo-link-target-verify-function 'org-entry-is-todo-p
  "Function to verify if the heading at point should be a completion target."
  :group 'org-todo-link
  :type '(choice
          (const :tag "All headings" nil)
          (const :tag "All headings with any keyword" org-get-todo-state)
          (const :tag "All headings with a todo keyword" org-entry-is-todo-p)
          (function)))

;;;; Faces

(defface org-todo-link-button
  '((((background light))
     (:underline nil :box
                 (:line-width
                  (1 . 1)
                  :color "light gray" :style flat-button)
                 :background "gainsboro"))
    (((background dark))
     (:underline nil :box
                 (:line-width
                  (1 . 1)
                  :color "gray35" :style flat-button)
                 :background "black")))
  "Face for TODO keywords.")

(defface org-todo-link-todo
  '((t :inherit (org-todo-link-button org-todo)))
  "Face for TODO items.")

(defface org-todo-link-done
  '((t :inherit (org-todo-link-button org-done)))
  "Face for DONE items.")

(defface org-todo-link-broken
  '((t :inherit (org-todo-link-button org-warning)))
  "Face for broken links.")

;; Set file link options
(org-link-set-parameters "file"
                         :activate-func #'org-todo-link-activate)

;; Set id link options
(org-link-set-parameters "id"
                         :activate-func #'org-todo-link-activate)

;;;; Org todo link keymap

(defun org-todo-link-recalculate ()
  "Recalculate TODO status for todo link at point."
  (interactive)
  (if-let ((ov (seq-find
                (lambda (o) (overlay-get o 'org-todo-link))
                (overlays-at (point)))))
      (if-let ((link (org-todo-link--get-link)))
          (overlay-put ov 'before-string (org-todo-link--get-todo (org-todo-link--find link)))
        (delete-overlay ov))))

(defun org-todo-link-todo (&optional arg)
  "Set TODO keyword on todo link at point.

If ARG is not provided, prompt for a TODO keyword."
  (interactive "P")
  (if-let ((ov (seq-find
                (lambda (o) (overlay-get o 'org-todo-link))
                (overlays-at (point)))))
      (if-let ((link (org-todo-link--get-link)))
          (let ((marker (org-todo-link--find link)))
            (if marker
                (with-current-buffer (marker-buffer marker)
                  (save-excursion
                    (goto-char (marker-position marker))
                    (org-todo arg))))
            (overlay-put ov 'before-string (org-todo-link--get-todo marker)))
        (delete-overlay ov))))

(defun org-todo-link-todo-nextset ()
  "Switch to the next set of TODO keywords on the target todo item."
  (interactive)
  (org-todo-link-todo 'nextset))

(defun org-todo-link-todo-previousset ()
  "Switch to the previous set of TODO keywords on the target todo item."
  (interactive)
  (org-todo-link-todo 'previousset))

(defun org-todo-link-todo-next ()
  "Switch to the next TODO keyword on the target todo item."
  (interactive)
  (org-todo-link-todo 'right))

(defun org-todo-link-todo-previous ()
  "Switch to the previous TODO keyword on the target todo item."
  (interactive)
  (org-todo-link-todo 'left))

(defvar org-todo-link-keymap
  (let ((map (make-sparse-keymap)))
    (mapc
     (lambda (k) (define-key map (kbd (car k)) (cdr k)))
     '(("C-c C-c" . org-todo-link-recalculate)
       ("C-c C-t" . org-todo-link-todo)
       ("S-<right>" . org-todo-link-todo-next)
       ("S-<left>" . org-todo-link-todo-previoius)
       ("C-S-<right>" . org-todo-link-todo-nextset)
       ("C-S-<left>" . org-todo-link-todo-previousset)))
    map)
  "Keymap for todo links.")

;; Useful in org capture templates for retrieving a todo item
;;;###autoload
(defun org-todo-link-query (&optional prompt)
  "Query for a file+heading link and return it as a string.

Uses PROMPT if specified when retrieving location."
  (let* ((org-refile-targets (or org-todo-link-completion-targets org-refile-targets))
         (org-refile-target-verify-function org-todo-link-target-verify-function)
         (it (org-refile-get-location (or prompt "TODO Item: ")))
         initial-input org-stored-links)
    (with-current-buffer (find-file-noselect (nth 1 it))
      (save-excursion
        (goto-char (nth 3 it))
        (setq initial-input (org-entry-get nil "ITEM"))
        (org-store-link '(16) t)))
    (if-let ((url (caar org-stored-links)))
        (let ((desc (read-string "Description: " initial-input)))
          (if (string-empty-p desc)
              (concat "[[" url "]]")
            (concat "[[" url "][" desc "]]"))))))


;;;###autoload
(defun org-todo-link-insert (&optional prompt)
  "Insert a link at point to a todo item with optional PROMPT."
  (interactive)
  (insert (org-todo-link-query prompt)))

;;;; org-todo-link-mode

;;;###autoload
(define-minor-mode org-todo-link-mode
  "Display TODO keyword, if it exists, for all file+heading links."
  :lighter " todo-link"
  (org-restart-font-lock))

(defun org-todo-link-activate (start end &rest _)
  "Create overlay from START to END with TODO keyword from heading at PATH."
  (let ((overlays (seq-filter
                   (lambda (o) (overlay-get o 'org-todo-link))
                   (overlays-in start end))))
    (if (or (not org-todo-link-mode)
            (eq major-mode 'org-agenda-mode))
        (mapc #'delete-overlay overlays)
      (if-let ((link (org-todo-link--get-link start)))
          (let ((marker (ignore-errors (org-todo-link--find link))))
            (if (not overlays)
                (let ((ov (make-overlay start end)))
                  (overlay-put ov 'org-todo-link t)
                  (overlay-put ov 'keymap org-todo-link-keymap)
                  (overlay-put ov 'evaporate t)
                  (overlay-put ov 'before-string (org-todo-link--get-todo marker)))
              (move-overlay (car overlays) start end)
              (overlay-put (car overlays) 'before-string (org-todo-link--get-todo marker))
              (mapc #'delete-overlay (cdr overlays))))
        (mapc #'delete-overlay overlays)))))

;;;; Utility Expressions

(defun org-todo-link--get-link (&optional pos)
  "Return the link object at POS or point, if it exists."
  (if-let* ((start (car (if pos
                            (save-excursion
                              (goto-char pos)
                              (org-in-regexp org-link-any-re))
                          (org-in-regexp org-link-any-re))))
            (link (save-excursion
                    (goto-char start)
                    (org-element-link-parser)))
            (type (org-element-property :type link)))
      (pcase type
        ("id" link)
        ("file"
         (if (org-element-property :search-option link)
             link)))))

(defun org-todo-link--find (link)
  "Find a marker for LINK pointing to heading, if it exists."
  (let ((path (org-element-property :path link)))
    (pcase (org-element-property :type link)
      ("id"
       (org-id-find path t))
      ("file"
       (if-let ((search (org-element-property :search-option link))
                (org-link-search-must-match-exact-headline t))
           (with-current-buffer (find-file-noselect path)
             (org-with-wide-buffer
              (org-link-search search nil t)
              (point-marker))))))))

(defun org-todo-link--get-todo (marker)
  "Get TODO keyword at MARKER, if it exists."
  (let ((todo (and marker (org-entry-get marker "TODO"))))
    (if (or todo org-todo-link-always-display)
        (concat
         (button-buttonize
          (cond
           ((not marker)
            (propertize "BROKEN" 'face 'org-todo-link-broken))
           (todo
            (let ((done-keywords (with-current-buffer (marker-buffer marker)
                                   org-done-keywords)))
              (propertize todo 'face (if (member todo done-keywords)
                                         'org-todo-link-done
                                       'org-todo-link-todo))))
           (t
            (propertize "NONE" 'face 'org-todo-link-button)))
          (if marker
              `(lambda (_)
                 (if-let ((keyword (org-todo-link--menu ,marker)))
                     (org-todo-link-todo keyword)))
            (lambda (_)
              (user-error "%s" "Link points nowhere."))))
         (propertize " " 'face '(:underline nil))))))

(defun org-todo-link--menu (marker)
  "Open menu for choosing todo keyword for heading at MARKER."
  (let ((keywords (with-current-buffer (marker-buffer marker)
                    (cons org-not-done-keywords org-done-keywords))))
    (x-popup-menu
     t
     `("Set TODO keyword"
       ("NONE" ("NONE" . ""))
       ("TODO" ,@(mapcar
                  (lambda (k) (cons k k))
                  (car keywords)))
       ("DONE" ,@(mapcar
                  (lambda (k) (cons k k))
                  (cdr keywords)))))))


(provide 'org-todo-link)

;;; org-todo-link.el ends here
